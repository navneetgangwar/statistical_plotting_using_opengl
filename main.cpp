#ifdef __APPLE__
#include<GLUT/glut.h>
#else
#include<GL/glut.h>
#endif

#include<stdio.h>

float width=400;
float height=400;
float w_old=500;
float h_old = 400;
float angle = 60;

GLfloat vertexHome[] = {-0.5,-0.65,-0.5,-0.15,-0.1,-0.15,-0.1,-0.65,
                         0.1,-0.65,0.1,-0.15,0.5,-0.15,0.5,-0.65,
                         -0.8,0.15,-0.8,0.65,-0.4,0.65,-0.4,0.15,
                         -0.2,0.15,-0.2,0.65,0.2,0.65,0.2,0.15,
                         0.4,0.15,0.4,0.65,0.8,0.65,0.8,0.15};

char str1[] ="Bar Graph";
char str2[] = "Histogram";
char str3 [] = "Scatter Plot";
char str4[] = "line chart";
char str5 [] = "Pie Chart";

void texts(char *str,GLfloat x,GLfloat y,GLfloat z){

    glColor3f(0.0,1.0,0.0);
    glRasterPos3f(x , y ,z);
    for(int i=0;i<strlen(str);i++)
    {   if(width/w_old>1.1)
        glutBitmapCharacter( GLUT_BITMAP_HELVETICA_18, str[i] );
        else
        glutBitmapCharacter( GLUT_BITMAP_HELVETICA_12, str[i] );
    }



}

void reshape(int w,int h)
{
    //starting coordinates and right and top

    width = w;
    height = h;
    glViewport(0,0,width,height);

    //creating a persepective
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();
    gluPerspective(angle,(float)width/(float)height,0.0,0.0);
    glMatrixMode(GL_MODELVIEW);

}

void display()
{
    glClear(GL_COLOR_BUFFER_BIT);

     ///clearing everything from buhffer
    glLoadIdentity();

    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(2,GL_FLOAT,2*sizeof(GLfloat),vertexHome);
    glColor3f(0.0,0.0,0.0);
    glDrawArrays(GL_QUADS,0,20);
    glDisableClientState(GL_VERTEX_ARRAY);

    ///writing texts
    texts(str1,-0.44,-0.45,0.0);
    texts(str2,0.16,-0.45,0.0);
    texts(str3,-0.76,0.35,0.0);
    texts(str4,-0.145,0.35,0.0);
    texts(str5,0.46,0.35,0.0);




    ///swapping the buffer for displaying the data
    glutSwapBuffers();

}

void initOpengl()
{
    glClearColor(0.6,0.0,0,0.6);
}

void homescreen()
{

}
int main(int argc,char **argv)
{
    //starting the glut
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowSize(width,height);
    glutCreateWindow("plotter");
    initOpengl();
    //callback function
    glutDisplayFunc(display);
    glutIdleFunc(display);
    glutReshapeFunc(reshape);
    glutMainLoop();

    return 0;

}
